from django.db import models

class User(models.Model):
    username = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=100)

    def __str__(self):
        return "{first_name} - {username}".format(first_name=self.name, username=self.username)
