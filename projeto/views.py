from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings
from easy_pdf.views import PDFTemplateView
from django.views.generic import View
from easy_pdf.rendering import render_to_pdf
from .forms import UserForm
from .models import User

def save_user(request):
    form = UserForm(request.POST or None)
    if form.is_valid():
        form.save()
        return HttpResponse('Formulário salvo')
    return render(request, 'form.html', {'form': form})

class HelloPDFView(PDFTemplateView):
    template_name = 'exibir.html'

    base_url = 'file://'+ settings.STATIC_URL
    download_filename = 'exibir.pdf'

    def get_context_data(self, **kwargs):
        users = User.objects.all()
        return super(HelloPDFView, self).get_context_data(
            pagesize='A4',
            title='Hi There',
            users=users,
            **kwargs,
        )

class PDFView(View):
    def get(self, request, *args, **kwargs):
        users = User.objects.all()
        return render_to_pdf(request, 'exibir.html', users)
